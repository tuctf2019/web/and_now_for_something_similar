<?php
	include("con.php");
   	session_start();

  	$username = $_POST["username"];
  	$password = $_POST["password"];
  	$query = "SELECT password FROM users WHERE user='$username' AND password='$password'";

	//check for the following types of queries
	$pattern = "/.*['\"].*OR.*|.*UNION.*|.*UPDATE.*|.*SELECT.*|.*DELETE.*|.*ALTER.*|.*CREATE.*|.*ADD.*|.*BACKUP.*|.*DROP.*|.*EXEC.*|.*INDEX.*|.*SET.*|.*TRUNCATE.*|.*VIEW.*|.*SLEEP.*|.*SHOW.*/i";
	$pattern2 = "/.*PASSWORD.*/i";
	$user_match = preg_match($pattern, $username);
	$user2_match = preg_match($pattern2, $username);
	//since OR is in the word passwORd, a field in the table, we check for word, and subtract in that case
	$user_match = (int)$user_match - (int)$user2_match;
  	$password_match = preg_match($pattern, $password);
	$password2_match = preg_match($pattern2, $password);
	$password_match = (int)$password_match - (int)$password2_match;
	//so if user uses admin or root or test as the user = bad, prev that
	$admin_pattern = "/.*ADMIN.*|.*ROOT.*|.*TEST.*/i";
	$admin_match1 = preg_match($admin_pattern, $username);
	$admin_match2 = preg_match($admin_pattern, $password);
	//now it's time to check if they used those hacks
  	if($user_match + $password_match != 0)  {
		echo '<link href="login.css" rel="stylesheet" type="text/css">';
    		echo "<h1>Whoops! Someone noticed your SQL Injection. Try again!</h1>";
  	}
	else if ($admin_match1 + $admin_match2 != 0){
		echo '<link href="login.css" rel="stylesheet" type="text/css">';
                echo "<h1>This action is not permitted. This will be reported to administrators.</h1>";
	}
  	else {
    		mysqli_multi_query($conn,$query);
		$success = False;
                do{

		    $result = mysqli_store_result($conn);
    		    $row = mysqli_fetch_array($result,MYSQLI_NUM);
		    //make sure they actually log in
    		    if ($row and ($row[0] == $password)) {
			echo "<title>You found it!</title>";
                        echo '<link href="login.css" rel="stylesheet" type="text/css">';
                        echo "<h1>$FLAG</h1>";
			$success = True;
		    }
		    mysqli_free_result($result);
		} while(mysqli_next_result($conn));
		if (!$success){
                    echo '<link href="login.css" rel="stylesheet" type="text/css">';
                    echo "<h1>Login failed.</h1>";
		}
  	}

?>
