# And Now, For Something Similar

So basically this is the `INSERT INTO` SQLi chal. Gotta create a user then login as them to get the flag. Chal Prereqs: Login to Access

* Desc: We made another attempt to login securely. Is this one any better than the last?
* Flag: `TUCTF{m4k3_y0ur53lf_4_u53r:_th3_l4zy_50lut10n?}`

## How To:
1. Hey look, is login page! SQLi is a go. Can't do `' or 1=1`, can't do `union select`...hmmm
2. So we know that we have the normal query structure based on the hint in the description and the `login.php.bak` from Login to Access (`user`, `password` columns, table is called `users`). This is where it gets fun
3. So I actually allowed multiquery...so you can create a user to login as. Can't be `admin`, `root` or `test` because those are easily guessed and can lead to someone guessing correct creds (I also require both a valid username and password so don't go trying the `admin'` tactic).
4. A query like "`'; insert into users (user, password) values ("a", "b");-- `" will insert a user with username `a` and password `b`. Note: this will return a `Login failed` page, but that doesn't mean the table wasn't changed! Also don't forget the two spaces after the comment (you don't _actually_ need the second semicolon but aethetic)
5. Now simply login with those creds you inserted into the table, and you've got the flag!
